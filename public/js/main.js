'use strict';

let catsObject;

document.querySelector('form').addEventListener('submit', (evt) => {
    evt.preventDefault();
    console.log(evt.target);
    const formData = new FormData(evt.target);
    let method;
    if(formData.get('_id')!=='') {
        console.log('update');
        method = 'put';
    } else {
        console.log('create');
        method = 'post';
    }
    const url = '/cats';
    fetch(url, {
            method: method,
            body: formData,
        }
    ).then((resp) => {
        return resp.json();
    }).then((json) => {
        console.log(json);
        emptyForm();
        getCats();
    });
});

const emptyForm = () => {
    const form = document.getElementById('formCat');
    form.querySelector('input[name=_id]').value = '';
    form.querySelector('input[name=name]').value = '';
    form.querySelector('input[name=age]').value = '';
    form.querySelector('select[name=gender]').value = '';
    form.querySelector('input[name=color]').value = '';
    form.querySelector('input[name=weight]').value = '';
};

const loadCatData = (id) => {
    console.log('Display cat ID ' + id);
    const catData = getCatDataById(id);
    console.log(catData);
    const form = document.getElementById('formCat');
    form.querySelector('input[name=_id]').value = catData._id;
    form.querySelector('input[name=name]').value = catData.name;
    form.querySelector('input[name=age]').value = catData.age;
    form.querySelector('select[name=gender]').value = catData.gender;
    form.querySelector('input[name=color]').value = catData.color;
    form.querySelector('input[name=weight]').value = catData.weight;
};

const getCatDataById = (id) => {
    return catsObject.filter((cat) => cat._id == id)[0];
}

const deleteCatById = (id) => {
    const url = '/delete/cat/' + id;
    fetch(url, {method: 'delete'}).then((resp) => {
        return resp.json();
    }).then((json) => {
        console.log(json);
        getCats();
    });
}

const getCats = () => {
    const url = '/cats';
    fetch(url)
        .then((resp) => {
            return resp.json();
        })
        .then((cats) => {
            catsObject = cats;
            console.log(catsObject);
            const catDiv = document.querySelector('#cats');
            catDiv.innerHTML='';
            for (const cat of cats) {
                const article = document.createElement('article');
                article.innerHTML = `
                                     <p>${cat.name}
                                     <br>${cat.gender}</p>
                                     <p>${cat.age} years</p>
                                     <p>${cat.weight} kg</p>
                                     <p><button class="updateCatButton" data-id="${cat._id}">Update</button><button class="deleteCatButton" data-id="${cat._id}">Delete</button></p>
                                     <hr>
                                     `;
                catDiv.appendChild(article);
        }
        const updateButtons = document.querySelectorAll('.updateCatButton');
        updateButtons.forEach((button) => {
            button.addEventListener('click', (event) => {
                const catId = event.currentTarget.getAttribute('data-id');
                loadCatData(catId);
            });
        });

        const deleteButtons = document.querySelectorAll('.deleteCatButton');
        deleteButtons.forEach((button) => {
            button.addEventListener('click', (event) => {
                const catId = event.currentTarget.getAttribute('data-id');
                deleteCatById(catId);
            });
        });
    });
};

getCats();